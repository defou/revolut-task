This is solution for revolut coding task
## Getting started
Run jar in **target** folder
```shell
java -jar coding_task_runnable.jar
```
## Application info
By default server starts at port 8000. This could be changed by command line arguments. Possible arguments are printed 
in console. Server accepts http request with predefined methods and urls, please find description below.
After running application database will create file for storing data in in the current working directory. 
## Used dependencies and libraries
- com.sun.net.httpserver.HttpServer
- H2 database
- Maven
- TestNg, Mockito
- Gson
- Commons CLI (parsing cmd arguments)
## Api description
Returns account by given id or all, if id is not specified:
```
Request
Method: GET
URL: /api/accounts{/id}

Response
Body:
{
    "accounts":[
        {
            "id": 1,
            "balance": 0,
            "version": 0
        },
        ...
    ]
}
```
Deletes account by given id or all, if id is not specified:
```
Request
Method: DELETE
URL: /api/accounts{/id}

Response
Body: is empty
```
Creates new account with given balance 
```
Request
Method: POST
URL: /api/accounts{/id}
Body: 
{
    "balance": 0 
}

Response
Body: is empty
```
Updates account by given id or all, if id is not specified:
```
Request
Method: PUT
URL: /api/accounts{/id}
Body: 
{
    "balance": 0 
}

Response
Body: is empty
```
Performs transaction between accounts:
```
Request
Method: PUT
URL: /api/accounts/transaction
Body: 
{
    "fromAccountId": 1,
    "toAccountId": 2,
    "value":10
}

Response
Body: is empty
```