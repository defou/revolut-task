package revolut;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpServer;
import org.apache.commons.cli.*;
import org.h2.jdbcx.JdbcConnectionPool;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.sql.SQLException;
import java.util.concurrent.Executors;

/**
 * This class parses command line arguments, performs configuration of data base connection and starts the server
 */
public class App {
    private static final int MAX_CONNECTION = 10;
    private static final String PORT_ARGUMENT = "port";
    private static final String PORT_DEFAULT = "8000";
    private static final String DB_URL_ARGUMENT = "db_url";
    private static final String DB_URL_DEFAULT = "jdbc:h2:./coding_task";
    private static final String DB_USER_ARGUMENT = "db_user";
    private static final String DB_USER_DEFAULT = "sa";
    private static final String DB_PASSWORD_ARGUMENT = "db_password";
    private static final String DB_PASSWORD_DEFAULT = "";

    public static void main(String[] args) throws IOException, ParseException, SQLException {
        startServer(args);
    }

    public static HttpServer startServer(String[] args) throws ParseException, SQLException, IOException {
        CommandLine parsedOptions = parseOptions(args);

        String dbUrl = parsedOptions.getOptionValue(DB_URL_ARGUMENT, DB_URL_DEFAULT);
        JdbcConnectionPool cp = JdbcConnectionPool.create(
                dbUrl,
                parsedOptions.getOptionValue(DB_USER_ARGUMENT, DB_USER_DEFAULT),
                parsedOptions.getOptionValue(DB_PASSWORD_ARGUMENT, DB_PASSWORD_DEFAULT));
        cp.setMaxConnections(MAX_CONNECTION);

        AccountDao accountDao = new AccountDao(cp);
        accountDao.initTable();
        Gson gson = new Gson();

        HttpServer server =
                HttpServer.create(new InetSocketAddress(Integer.parseInt(parsedOptions.getOptionValue(PORT_ARGUMENT,
                        PORT_DEFAULT))), 0);
        server.setExecutor(Executors.newFixedThreadPool(MAX_CONNECTION));
        server.createContext(AccountsHttpHandler.HANDLE_URL, new AccountsHttpHandler(accountDao, gson));

        Runtime.getRuntime().addShutdownHook(new Thread(cp::dispose));
        server.start();
        System.out.println("Server started at port " + server.getAddress().getPort());
        return server;
    }

    private static CommandLine parseOptions(String[] args) throws ParseException {
        Options options = new Options()
                .addOption(Option.builder(PORT_ARGUMENT).hasArg(true).desc("Http server port").required(false)
                        .type(Integer.class).build())
                .addOption(Option.builder(DB_URL_ARGUMENT).hasArg(true).desc("H2 database connection url").build())
                .addOption(Option.builder(DB_USER_ARGUMENT).hasArg(true).desc("H2 database user name").build())
                .addOption(Option.builder(DB_PASSWORD_ARGUMENT).hasArg(true).desc("H2 database password").build());
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("next arguments could be used, all are optional", options);
        return parser.parse(options, args);
    }
}
