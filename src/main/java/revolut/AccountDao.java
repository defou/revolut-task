package revolut;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Data access object for account entity, performs basic crud operations, creates table for entity, handles money
 * transaction
 */
public class AccountDao {

    private final DataSource dataSource;

    public AccountDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private PreparedStatement preparedStatementForCreate(Connection conn, Account account) throws SQLException {
        PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO ACCOUNT (balance) VALUES (?)");
        preparedStatement.setBigDecimal(1, account.getBalance());
        return preparedStatement;
    }

    public void create(Account account) throws SQLException {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = preparedStatementForCreate(conn, account)) {
            preparedStatement.execute();
        }
    }

    public List<Account> readAll() throws SQLException {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement("SELECT ID, BALANCE, VERSION FROM Account");
             ResultSet resultSet = preparedStatement.executeQuery()) {
            return mapAccounts(resultSet);
        }
    }

    private PreparedStatement preparedStatementForRead(Integer id, Connection conn) throws SQLException {
        PreparedStatement preparedStatement = conn.prepareStatement("SELECT ID, BALANCE, VERSION FROM Account WHERE " +
                "ID = ?");
        preparedStatement.setInt(1, id);
        return preparedStatement;
    }

    public List<Account> read(Integer id) throws SQLException {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = preparedStatementForRead(id, conn);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            return mapAccounts(resultSet);
        }
    }


    public void deleteAll() throws SQLException {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement("DELETE FROM ACCOUNT")) {
            preparedStatement.execute();
        }
    }

    private PreparedStatement preparedStatementForDelete(int id, Connection conn) throws SQLException {
        PreparedStatement preparedStatement = conn.prepareStatement("DELETE FROM ACCOUNT WHERE ID = ?");
        preparedStatement.setInt(1, id);
        return preparedStatement;
    }

    public void delete(int id) throws SQLException {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = preparedStatementForDelete(id, conn)) {
            preparedStatement.execute();
        }
    }

    PreparedStatement preparedStatementForUpdate(Account account, Connection conn, boolean useVersion) throws SQLException {
        PreparedStatement preparedStatement = conn.prepareStatement("UPDATE ACCOUNT SET BALANCE = ?, VERSION = " +
                "VERSION + 1 WHERE ID = ? AND (? IS NULL OR ? = VERSION)");
        preparedStatement.setBigDecimal(1, account.getBalance());
        preparedStatement.setInt(2, account.getId());
        if (useVersion) {
            preparedStatement.setInt(3, account.getVersion());
            preparedStatement.setInt(4, account.getVersion());
        } else {
            preparedStatement.setNull(3, Types.INTEGER);
            preparedStatement.setNull(4, Types.INTEGER);
        }
        return preparedStatement;
    }

    public void update(Account account) throws SQLException {
        Connection conn = dataSource.getConnection();
        PreparedStatement preparedStatement = preparedStatementForUpdate(account, conn, false);

        preparedStatement.execute();
        preparedStatement.close();
        conn.close();
    }

    private PreparedStatement preparedStatementForUpdateAll(Account account, Connection conn) throws SQLException {
        PreparedStatement preparedStatement = conn.prepareStatement("UPDATE ACCOUNT SET BALANCE = ?, VERSION = " +
                "VERSION + 1");
        preparedStatement.setBigDecimal(1, account.getBalance());
        return preparedStatement;
    }

    public void updateAll(Account account) throws SQLException {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = preparedStatementForUpdateAll(account, conn)) {
            preparedStatement.execute();
        }
    }

    /**
     * This method performs money transaction between accounts. Firstly, two accounts are read from database,
     * the checks balance. If check is complete, then updates account with optimistic locking. If update of any account
     * fails, rollbacks transaction.
     */
    public void doTransaction(Transaction transaction) throws Exception {
        Connection conn = dataSource.getConnection();
        try {
            conn.setAutoCommit(false);

            Account accountFrom;
            try (PreparedStatement preparedStatementFrom = preparedStatementForRead(transaction.getFromAccountId(),
                    conn);
                 ResultSet resultSetFrom = preparedStatementFrom.executeQuery()) {
                accountFrom = mapAccounts(resultSetFrom).get(0);
            }
            Account accountTo;
            try (PreparedStatement preparedStatementTo = preparedStatementForRead(transaction.getToAccountId(), conn);
                 ResultSet resultSetTo = preparedStatementTo.executeQuery()) {
                accountTo = mapAccounts(resultSetTo).get(0);
            }

            if (accountFrom.getBalance().compareTo(transaction.getValue()) >= 0) {
                accountFrom.setBalance(accountFrom.getBalance().subtract(transaction.getValue()));
                accountTo.setBalance(accountTo.getBalance().add(transaction.getValue()));
                List<Account> updateList = List.of(accountFrom, accountTo).stream()
                        .sorted(Comparator.comparingInt(Account::getId)).collect(Collectors.toList());
                for (Account account : updateList) {
                    try (PreparedStatement preparedStatement = preparedStatementForUpdate(account, conn, true)) {
                        if (preparedStatement.executeUpdate() != 1) {
                            throw new IllegalStateException("Account was modified during money transfer");
                        }
                    }
                }
                conn.commit();
            } else {
                conn.rollback();
            }
        } catch (Exception e) {
            conn.rollback();
            throw e;
        } finally {
            conn.close();
        }
    }

    private List<Account> mapAccounts(ResultSet resultSet) throws SQLException {
        List<Account> accounts = new ArrayList<>();
        while (resultSet.next()) {
            accounts.add(new Account(resultSet.getInt(1), resultSet.getBigDecimal(2), resultSet.getInt(3)));
        }
        return accounts;
    }

    public void initTable() throws SQLException {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(
                     "CREATE TABLE IF NOT EXISTS ACCOUNT " +
                             "(ID INT PRIMARY KEY AUTO_INCREMENT, " +
                             "BALANCE DECIMAL NOT NULL DEFAULT 0, " +
                             "VERSION INT DEFAULT 0 )")) {
            preparedStatement.execute();
        }
    }
}
