package revolut;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Entity class for account
 */
public class Account {
    private int id;
    private BigDecimal balance;
    private int version;

    public Account(int id, BigDecimal balance, int version) {
        this.id = id;
        this.balance = balance;
        this.version = version;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id &&
                version == account.version &&
                Objects.equals(balance, account.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, balance, version);
    }
}
