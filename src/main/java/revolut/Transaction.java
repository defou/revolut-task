package revolut;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Model for representation of money transaction. Is not stored in data base
 */
public class Transaction {
    private int fromAccountId;
    private int toAccountId;
    private BigDecimal value;

    public Transaction(int fromAccountId, int toAccountId, BigDecimal value) {
        this.fromAccountId = fromAccountId;
        this.toAccountId = toAccountId;
        this.value = value;
    }

    public int getFromAccountId() {
        return fromAccountId;
    }

    public void setFromAccountId(int fromAccountId) {
        this.fromAccountId = fromAccountId;
    }

    public int getToAccountId() {
        return toAccountId;
    }

    public void setToAccountId(int toAccountId) {
        this.toAccountId = toAccountId;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return fromAccountId == that.fromAccountId &&
                toAccountId == that.toAccountId &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fromAccountId, toAccountId, value);
    }
}
