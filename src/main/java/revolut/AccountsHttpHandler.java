package revolut;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.Optional;

/**
 * Handles http requests for account api, maps http methods to dau functions, handles error, parses entity to json
 */
public class AccountsHttpHandler implements HttpHandler {

    public static final String HANDLE_URL = "/api/accounts";
    private final AccountDao accountDao;
    private final Gson gson;

    public AccountsHttpHandler(AccountDao accountDao, Gson gson) {
        this.accountDao = accountDao;
        this.gson = gson;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        String responseBody = "";
        int responseCode = 200;
        try {
            switch (exchange.getRequestMethod()) {
                case "GET":
                    responseBody = get(exchange);
                    break;
                case "DELETE":
                    delete(exchange);
                    break;
                case "POST":
                    post(exchange);
                    break;
                case "PUT":
                    put(exchange);
                    break;
                default:
                    responseBody = "Method is not allowed";
                    responseCode = 405;
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseBody = getErrorBody(e.getMessage());
            responseCode = 500;
        }
        exchange.getResponseHeaders().add("Content-Type", "application/json");
        byte[] bytes = responseBody.getBytes();
        exchange.sendResponseHeaders(responseCode, bytes.length);
        writeBody(exchange, bytes);
    }

    String get(HttpExchange httpExchange) throws SQLException {
        JsonObject response = new JsonObject();
        Optional<Integer> id = getId(httpExchange.getRequestURI().getPath());
        if (id.isPresent()) {
            response.add("accounts", gson.toJsonTree(accountDao.read(id.get())));
        } else {
            response.add("accounts", gson.toJsonTree(accountDao.readAll()));
        }
        return response.toString();
    }

    void delete(HttpExchange httpExchange) throws SQLException {
        Optional<Integer> id = getId(httpExchange.getRequestURI().getPath());
        if (id.isPresent()) {
            accountDao.delete(id.get());
        } else {
            accountDao.deleteAll();
        }
    }

    void post(HttpExchange exchange) throws IOException, SQLException {
        String body = readBody(exchange);
        Account account = gson.fromJson(body, Account.class);
        accountDao.create(account);
    }

    void put(HttpExchange exchange) throws Exception {
        if (exchange.getRequestURI().getPath().matches(HANDLE_URL + "/transaction")) {
            Transaction transaction = gson.fromJson(readBody(exchange), Transaction.class);
            accountDao.doTransaction(transaction);
        } else {
            Optional<Integer> id = getId(exchange.getRequestURI().getPath());
            if (id.isPresent()) {
                final Account account = gson.fromJson(readBody(exchange), Account.class);
                account.setId(id.get());
                accountDao.update(account);
            } else {
                accountDao.updateAll(gson.fromJson(readBody(exchange), Account.class));
            }
        }

    }

    String getErrorBody(String message) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("error", new JsonPrimitive(message));
        return jsonObject.toString();
    }

    Optional<Integer> getId(String path) {
        if (!HANDLE_URL.equals(path)) {
            String idString = path.substring(HANDLE_URL.length() + 1);
            return Optional.of(Integer.parseUnsignedInt(idString.split("/")[0]));
        }
        return Optional.empty();
    }

    String readBody(HttpExchange httpExchange) throws IOException {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        InputStream inputStream = httpExchange.getRequestBody();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }
        inputStream.close();
        return result.toString(StandardCharsets.UTF_8);
    }

    void writeBody(HttpExchange exchange, byte[] bytes) throws IOException {
        OutputStream os = exchange.getResponseBody();
        os.write(bytes);
        os.close();
    }
}
