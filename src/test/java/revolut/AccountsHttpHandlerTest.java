package revolut;

import com.google.gson.Gson;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.sql.SQLException;
import java.util.List;

import static org.testng.Assert.*;

public class AccountsHttpHandlerTest {

    private AccountsHttpHandler accountsHttpHandler;
    Gson gson;
    @Mock
    private AccountDao accountDao;
    @Mock
    HttpExchange httpExchange;
    @Mock
    Headers headers;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
        gson = new Gson();
        accountsHttpHandler = new AccountsHttpHandler(accountDao, gson);
        Mockito.when(httpExchange.getResponseHeaders()).thenReturn(headers);
    }

    @Test
    public void testHandleGet() throws IOException, SQLException {
        Mockito.when(httpExchange.getRequestMethod()).thenReturn("GET");
        AccountsHttpHandler spy = Mockito.spy(this.accountsHttpHandler);
        Mockito.doReturn("response body").when(spy).get(Mockito.any());
        Mockito.doNothing().when(spy).writeBody(Mockito.any(), Mockito.any());

        spy.handle(httpExchange);

        Mockito.verify(spy).get(httpExchange);
        Mockito.verify(httpExchange).sendResponseHeaders(Mockito.eq(200), Mockito.anyInt());
        Mockito.verify(spy).writeBody(Mockito.any(), Mockito.any());

    }

    @Test
    public void testHandlePost() throws IOException, SQLException {
        Mockito.when(httpExchange.getRequestMethod()).thenReturn("POST");
        AccountsHttpHandler spy = Mockito.spy(this.accountsHttpHandler);
        Mockito.doNothing().when(spy).post(Mockito.any());
        Mockito.doNothing().when(spy).writeBody(Mockito.any(), Mockito.any());

        spy.handle(httpExchange);

        Mockito.verify(spy).post(httpExchange);
        Mockito.verify(httpExchange).sendResponseHeaders(Mockito.eq(200), Mockito.anyInt());
        Mockito.verify(spy).writeBody(Mockito.any(), Mockito.any());
    }

    @Test
    public void testHandlePut() throws Exception {
        Mockito.when(httpExchange.getRequestMethod()).thenReturn("PUT");
        AccountsHttpHandler spy = Mockito.spy(this.accountsHttpHandler);
        Mockito.doNothing().when(spy).put(Mockito.any());
        Mockito.doNothing().when(spy).writeBody(Mockito.any(), Mockito.any());

        spy.handle(httpExchange);

        Mockito.verify(spy).put(httpExchange);
        Mockito.verify(httpExchange).sendResponseHeaders(Mockito.eq(200), Mockito.anyInt());
        Mockito.verify(spy).writeBody(Mockito.any(), Mockito.any());
    }

    @Test
    public void testHandleDelete() throws IOException, SQLException {
        Mockito.when(httpExchange.getRequestMethod()).thenReturn("DELETE");
        AccountsHttpHandler spy = Mockito.spy(this.accountsHttpHandler);
        Mockito.doNothing().when(spy).delete(Mockito.any());
        Mockito.doNothing().when(spy).writeBody(Mockito.any(), Mockito.any());

        spy.handle(httpExchange);

        Mockito.verify(spy).delete(httpExchange);
        Mockito.verify(httpExchange).sendResponseHeaders(Mockito.eq(200), Mockito.anyInt());
        Mockito.verify(spy).writeBody(Mockito.any(), Mockito.any());
    }

    @Test
    public void testGet() throws SQLException {
        URI uri = URI.create("http://localhost:8000/api/accounts/123");
        Mockito.when(httpExchange.getRequestURI()).thenReturn(uri);
        Mockito.when(accountDao.read(Mockito.anyInt())).thenReturn(List.of(new Account(123, BigDecimal.valueOf(555),
                0)));

        String response = accountsHttpHandler.get(httpExchange);
        Assert.assertEquals(response, "{\"accounts\":[{\"id\":123,\"balance\":555,\"version\":0}]}");
    }

    @Test
    public void testGetAll() throws SQLException {
        URI uri = URI.create("http://localhost:8000/api/accounts");
        Mockito.when(httpExchange.getRequestURI()).thenReturn(uri);
        Mockito.when(accountDao.readAll()).thenReturn(List.of(new Account(123, BigDecimal.valueOf(555),
                0)));

        String response = accountsHttpHandler.get(httpExchange);
        Assert.assertEquals(response, "{\"accounts\":[{\"id\":123,\"balance\":555,\"version\":0}]}");
    }

    @Test
    public void testDelete() throws SQLException {
        URI uri = URI.create("http://localhost:8000/api/accounts/1");
        Mockito.when(httpExchange.getRequestURI()).thenReturn(uri);

        accountsHttpHandler.delete(httpExchange);

        Mockito.verify(accountDao).delete(1);
    }

    @Test
    public void testDeleteAll() throws SQLException {
        URI uri = URI.create("http://localhost:8000/api/accounts");
        Mockito.when(httpExchange.getRequestURI()).thenReturn(uri);

        accountsHttpHandler.delete(httpExchange);

        Mockito.verify(accountDao).deleteAll();
    }

    @Test
    public void testPost() throws IOException, SQLException {
        AccountsHttpHandler spy = Mockito.spy(accountsHttpHandler);
        Mockito.doReturn("{\"id\":123,\"balance\":555,\"version\":0}").when(spy).readBody(Mockito.any());

        spy.post(httpExchange);

        Mockito.verify(accountDao).create(Mockito.eq(new Account(123, BigDecimal.valueOf(555), 0)));
    }

    @Test
    public void testPut() throws Exception {
        AccountsHttpHandler spy = Mockito.spy(accountsHttpHandler);
        URI uri = URI.create("http://localhost:8000/api/accounts/1");
        Mockito.when(httpExchange.getRequestURI()).thenReturn(uri);
        Mockito.doReturn("{\"balance\":555}").when(spy).readBody(Mockito.any());

        spy.put(httpExchange);

        Mockito.verify(accountDao).update(Mockito.eq(new Account(1, BigDecimal.valueOf(555), 0)));
    }

    @Test
    public void testPutAll() throws Exception {
        AccountsHttpHandler spy = Mockito.spy(accountsHttpHandler);
        URI uri = URI.create("http://localhost:8000/api/accounts");
        Mockito.when(httpExchange.getRequestURI()).thenReturn(uri);
        Mockito.doReturn("{\"id\":123,\"balance\":555,\"version\":0}").when(spy).readBody(Mockito.any());

        spy.put(httpExchange);

        Mockito.verify(accountDao).updateAll(Mockito.eq(new Account(123, BigDecimal.valueOf(555), 0)));
    }

    @Test
    public void testPutTransaction() throws Exception {
        AccountsHttpHandler spy = Mockito.spy(accountsHttpHandler);
        URI uri = URI.create("http://localhost:8000/api/accounts/transaction");
        Mockito.when(httpExchange.getRequestURI()).thenReturn(uri);
        Mockito.doReturn("{\"fromAccountId\":111,\"toAccountId\":222,\"value\":50}").when(spy).readBody(Mockito.any());

        spy.put(httpExchange);

        Mockito.verify(accountDao).doTransaction(Mockito.eq(new Transaction(111, 222, BigDecimal.valueOf(50))));
    }


}