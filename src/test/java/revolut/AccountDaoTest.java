package revolut;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.*;
import java.util.List;

public class AccountDaoTest {

    private AccountDao accountDao;
    @Mock
    private DataSource dataSource;
    @Mock
    private Connection connection;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private ResultSet resultSet;


    @BeforeMethod
    public void init() throws SQLException {
        MockitoAnnotations.initMocks(this);
        accountDao = new AccountDao(dataSource);
        Mockito.when(dataSource.getConnection()).thenReturn(connection);
        Mockito.when(connection.prepareStatement(Mockito.anyString())).thenReturn(preparedStatement);
        Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);
    }

    @Test
    public void testCreate() throws SQLException {
        accountDao.create(new Account(2, BigDecimal.valueOf(2), 3));

        Mockito.verify(connection).prepareStatement("INSERT INTO ACCOUNT (balance) VALUES (?)");
        Mockito.verify(preparedStatement).setBigDecimal(1, BigDecimal.valueOf(2));

        Mockito.verify(preparedStatement).close();
        Mockito.verify(connection).close();
    }

    @Test
    public void testReadAll() throws SQLException {
        Mockito.when(resultSet.next()).thenReturn(true).thenReturn(false);
        Mockito.when(resultSet.getInt(1)).thenReturn(111);
        Mockito.when(resultSet.getBigDecimal(2)).thenReturn(BigDecimal.valueOf(222));
        Mockito.when(resultSet.getInt(3)).thenReturn(333);

        List<Account> accounts = accountDao.readAll();

        Mockito.verify(connection).prepareStatement("SELECT ID, BALANCE, VERSION FROM Account");
        Mockito.verify(preparedStatement).close();
        Mockito.verify(connection).close();
        Assert.assertEquals(accounts.get(0), new Account(111, BigDecimal.valueOf(222), 333));
    }

    @Test
    public void testRead() throws SQLException {
        Mockito.when(resultSet.next()).thenReturn(true).thenReturn(false);
        Mockito.when(resultSet.getInt(1)).thenReturn(111);
        Mockito.when(resultSet.getBigDecimal(2)).thenReturn(BigDecimal.valueOf(222));
        Mockito.when(resultSet.getInt(3)).thenReturn(333);

        List<Account> accounts = accountDao.read(123);

        Mockito.verify(connection).prepareStatement("SELECT ID, BALANCE, VERSION FROM Account WHERE ID = ?");
        Mockito.verify(preparedStatement).setInt(1, 123);
        Mockito.verify(preparedStatement).close();
        Mockito.verify(connection).close();
        Assert.assertEquals(accounts.get(0), new Account(111, BigDecimal.valueOf(222), 333));
    }

    @Test
    public void testDeleteAll() throws SQLException {
        accountDao.deleteAll();

        Mockito.verify(connection).prepareStatement("DELETE FROM ACCOUNT");
        Mockito.verify(preparedStatement).close();
        Mockito.verify(connection).close();
    }

    @Test
    public void testDelete() throws SQLException {
        accountDao.delete(123);

        Mockito.verify(connection).prepareStatement("DELETE FROM ACCOUNT WHERE ID = ?");
        Mockito.verify(preparedStatement).setInt(1, 123);
        Mockito.verify(preparedStatement).close();
        Mockito.verify(connection).close();
    }

    @Test
    public void testUpdate() throws SQLException {
        accountDao.update(new Account(1, BigDecimal.valueOf(2), 3));

        Mockito.verify(connection).prepareStatement("UPDATE ACCOUNT SET BALANCE = ?, VERSION = " +
                "VERSION + 1 WHERE ID = ? AND (? IS NULL OR ? = VERSION)");
        Mockito.verify(preparedStatement).setBigDecimal(1, BigDecimal.valueOf(2));
        Mockito.verify(preparedStatement).setInt(2, 1);
        Mockito.verify(preparedStatement).setNull(3, Types.INTEGER);
        Mockito.verify(preparedStatement).setNull(4, Types.INTEGER);
        Mockito.verify(preparedStatement).close();
        Mockito.verify(connection).close();
    }

    @Test
    public void testUpdateAll() throws SQLException {
        accountDao.updateAll(new Account(1, BigDecimal.valueOf(2), 3));

        Mockito.verify(connection).prepareStatement("UPDATE ACCOUNT SET BALANCE = ?, VERSION = VERSION + 1");
        Mockito.verify(preparedStatement).setBigDecimal(1, BigDecimal.valueOf(2));
        Mockito.verify(preparedStatement).close();
        Mockito.verify(connection).close();
    }

    @Test
    public void testDoTransaction() throws Exception {
        final AccountDao spy = Mockito.spy(accountDao);
        Mockito.when(resultSet.next()).thenReturn(true, false, true, false);
        Mockito.when(resultSet.getInt(1)).thenReturn(111, 222);
        Mockito.when(resultSet.getBigDecimal(2)).thenReturn(BigDecimal.valueOf(1000), BigDecimal.valueOf(2000));
        Mockito.when(resultSet.getInt(3)).thenReturn(101, 202);
        Mockito.when(preparedStatement.executeUpdate()).thenReturn(1);

        spy.doTransaction(new Transaction(111, 222, BigDecimal.valueOf(333)));

        Mockito.verify(connection).setAutoCommit(false);
        Mockito.verify(spy, Mockito.times(2)).preparedStatementForUpdate(Mockito.any(Account.class),
                Mockito.eq(connection), Mockito.eq(true));
        Mockito.verify(connection).commit();
    }

    @Test
    public void testDoTransactionWithLowBalance() throws Exception {
        final AccountDao spy = Mockito.spy(accountDao);
        Mockito.when(resultSet.next()).thenReturn(true, false, true, false);
        Mockito.when(resultSet.getInt(1)).thenReturn(111, 222);
        Mockito.when(resultSet.getBigDecimal(2)).thenReturn(BigDecimal.valueOf(100), BigDecimal.valueOf(2000));
        Mockito.when(resultSet.getInt(3)).thenReturn(101, 202);
        Mockito.when(preparedStatement.executeUpdate()).thenReturn(1);

        spy.doTransaction(new Transaction(111, 222, BigDecimal.valueOf(333)));

        Mockito.verify(connection).setAutoCommit(false);
        Mockito.verify(connection).rollback();
    }

    @Test(expectedExceptions = IllegalStateException.class)
    public void testDoTransactionWithLocking() throws Exception {
        final AccountDao spy = Mockito.spy(accountDao);
        Mockito.when(resultSet.next()).thenReturn(true, false, true, false);
        Mockito.when(resultSet.getInt(1)).thenReturn(111, 222);
        Mockito.when(resultSet.getBigDecimal(2)).thenReturn(BigDecimal.valueOf(1000), BigDecimal.valueOf(2000));
        Mockito.when(resultSet.getInt(3)).thenReturn(101, 202);
        Mockito.when(preparedStatement.executeUpdate()).thenReturn(1, 0);

        spy.doTransaction(new Transaction(111, 222, BigDecimal.valueOf(333)));

        Mockito.verify(connection).setAutoCommit(false);
        Mockito.verify(connection).rollback();
    }

    @Test
    public void testInitTable() throws SQLException {
        accountDao.initTable();

        Mockito.verify(connection).prepareStatement("CREATE TABLE IF NOT EXISTS ACCOUNT " +
                "(ID INT PRIMARY KEY AUTO_INCREMENT, " +
                "BALANCE DECIMAL NOT NULL DEFAULT 0, " +
                "VERSION INT DEFAULT 0 )");
        Mockito.verify(preparedStatement).close();
        Mockito.verify(connection).close();
    }
}