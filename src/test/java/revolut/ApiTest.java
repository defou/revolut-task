package revolut;

import com.sun.net.httpserver.HttpServer;
import org.apache.commons.cli.ParseException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.SQLException;

import static revolut.TestUtils.sendHttp;

public class ApiTest {
    private HttpServer httpServer;

    @BeforeClass
    public void before() throws ParseException, SQLException, IOException {
        //Use inmemory db
        httpServer = App.startServer(new String[]{"-db_url", "jdbc:h2:mem:test"});
    }

    @AfterClass
    public void after() {
        httpServer.stop(0);
    }

    @AfterMethod
    public void afterMethod() throws IOException {
        sendHttp("http://localhost:8000/api/accounts", "DELETE", null);
    }

    @Test
    public void createAndGet() throws IOException {
        sendHttp("http://localhost:8000/api/accounts", "POST", "{\"balance\":100}");
        sendHttp("http://localhost:8000/api/accounts", "POST", "{\"balance\":200}");

        String allAccounts = sendHttp("http://localhost:8000/api/accounts", "GET", null);
        String single = sendHttp("http://localhost:8000/api/accounts/2", "GET", null);

        Assert.assertEquals(allAccounts, "{\"accounts\":[" +
                "{\"id\":1,\"balance\":100,\"version\":0}," +
                "{\"id\":2,\"balance\":200,\"version\":0}]}");
        Assert.assertEquals(single, "{\"accounts\":[{\"id\":2,\"balance\":200,\"version\":0}]}");
    }

    @Test(dependsOnMethods = "createAndGet")
    public void createAndUpdate() throws IOException {
        sendHttp("http://localhost:8000/api/accounts", "POST", "{\"balance\":100}");

        String before = sendHttp("http://localhost:8000/api/accounts/3", "GET", null);
        Assert.assertEquals(before, "{\"accounts\":[{\"id\":3,\"balance\":100,\"version\":0}]}");

        sendHttp("http://localhost:8000/api/accounts/3", "PUT", "{\"balance\":333}");

        String after = sendHttp("http://localhost:8000/api/accounts/3", "GET", null);
        Assert.assertEquals(after, "{\"accounts\":[{\"id\":3,\"balance\":333,\"version\":1}]}");
    }

    @Test(dependsOnMethods = "createAndUpdate")
    public void createAndDelete() throws IOException {
        sendHttp("http://localhost:8000/api/accounts", "POST", "{\"balance\":100}");
        sendHttp("http://localhost:8000/api/accounts", "POST", "{\"balance\":200}");
        sendHttp("http://localhost:8000/api/accounts", "POST", "{\"balance\":300}");

        sendHttp("http://localhost:8000/api/accounts/5", "DELETE", null);
        Assert.assertEquals(sendHttp("http://localhost:8000/api/accounts", "GET", null),
                "{\"accounts\":[" +
                        "{\"id\":4,\"balance\":100,\"version\":0}," +
                        "{\"id\":6,\"balance\":300,\"version\":0}]}");

        sendHttp("http://localhost:8000/api/accounts", "DELETE", null);
        Assert.assertEquals(sendHttp("http://localhost:8000/api/accounts", "GET", null),
                "{\"accounts\":[]}");
    }

    @Test(dependsOnMethods = "createAndDelete")
    public void transaction() throws IOException {
        sendHttp("http://localhost:8000/api/accounts", "POST", "{\"balance\":100}");
        sendHttp("http://localhost:8000/api/accounts", "POST", "{\"balance\":200}");
        sendHttp("http://localhost:8000/api/accounts/transaction", "PUT",
                "{\"fromAccountId\":7,\"toAccountId\":8,\"value\":70}");

        String allAccounts = sendHttp("http://localhost:8000/api/accounts", "GET", null);

        Assert.assertEquals(allAccounts, "{\"accounts\":[" +
                "{\"id\":7,\"balance\":30,\"version\":1}," +
                "{\"id\":8,\"balance\":270,\"version\":1}]}");
    }


}